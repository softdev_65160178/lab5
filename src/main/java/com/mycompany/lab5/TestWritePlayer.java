/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author natta
 */
public class TestWritePlayer {
    public static void main(String[] args) throws FileNotFoundException {
        ObjectOutputStream oos = null;
        try {
            Player x = new Player('X');
            Player o = new Player('O');
            x.win();
            o.loss();
            x.win();
            o.loss();
            x.draw();
            o.draw();
            System.out.println(x);
            System.out.println(o);
            File file = new File("players.dat");
            FileOutputStream fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.writeObject(x);
            oos.close();
            fos.close();
        } catch (IOException ex) {
            Logger.getLogger(TestWritePlayer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                oos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestWritePlayer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
